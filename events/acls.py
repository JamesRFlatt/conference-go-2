from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    picture_url = content["photos"][0]["url"]
    try:
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    geocoding_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geocoding_url, params=geocoding_params)

    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "http://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=weather_params)
    content = response.json()
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]

    try:
        return {"description": description, "temp": temp}
    except (KeyError, IndexError):
        return {
            "description": "weather data unavailable",
            "temp": "temperature unavailable",
        }
